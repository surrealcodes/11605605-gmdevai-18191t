﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Path {
    protected List<Node> nodes = new List<Node>();

    protected float length = 0;

    public virtual List<Node> GetNodes
    {
        get
        {
            return nodes;
        }
    }

    public virtual float GetLength
    {
        get
        {
            return length;
        }
    }

    // Make the path ready for usage, such as calculating the length
    public virtual void Bake()
    {
        // already calculated nodes
        List<Node> calculated = new List<Node>();
        length = 0;

        for (int i = 0; i < nodes.Count; i++)
        {
            // check every node per iteration
            Node node = nodes[i];

            // check target nodes of the node[i]
            for (int j = 0; j < node.GetConnections.Count; j++)
            {
                // target node of node[i]
                Node connection = node.GetConnections[j];

                // Don't calculate calculated nodes
                if (nodes.Contains(connection) && !calculated.Contains(connection))
                {
                    // Calculating the distance between a node and a connection when they are both available in the path nodes list
                    length += Vector3.Distance(node.transform.position, connection.transform.position);
                }

                // tag node[i] as already calculated
                calculated.Add(node);
            }
        }
    }

    public override string ToString()
    {
        // string formatting
        return string.Format("Nodes: {0}\nLength: {1}", string.Join(", ", nodes.Select(node => node.name).ToArray()), length);
        // join: continuous concat
        // select(node => node.name): gets node name
        // {x}: equiv to %s
    }
}
