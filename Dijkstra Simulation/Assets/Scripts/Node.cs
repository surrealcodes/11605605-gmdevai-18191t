﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Node : MonoBehaviour {

    [SerializeField]
    protected List<Node> connections = new List<Node>();

    public virtual List<Node> GetConnections
    {
        get
        {
            return connections;
        }
    }

    public Node this [int index]
    {
        get
        {
            return connections[index];
        }
    }

    private void OnValidate()
    {
        // remove duplicate elements
        connections = connections.Distinct().ToList(); // gets distinct elements and converts to list
    }
}
