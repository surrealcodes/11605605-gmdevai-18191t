﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Graph : MonoBehaviour {
    [SerializeField]
    protected List<Node> nodes = new List<Node>();

    public virtual List<Node> GetNodes
    {
        get
        {
            return nodes;
        }
    }

    public virtual Path GetDijkstraPath(Node start, Node end)
    {
        // We don't want null args
        if (start == null || end == null)
        {
            throw new ArgumentNullException();
        }

        // Final path
        Path path = new Path();

        // If start and end nodes are the same, we can return the start node
        // It is immediately understood that no motion occurs
        if (start == end)
        {
            path.GetNodes.Add(start);
            return path;
        }

        // unvisited nodes
        List<Node> unvisited = new List<Node>();

        // Previous nodes in optimal (cheapest) path from source
        Dictionary<Node, Node> previous = new Dictionary<Node, Node>();

        // The calculated distances
        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        // Set all nodes to inf except start node
        for (int i = 0; i < nodes.Count; i++)
        {
            Node node = nodes[i];

            // add all nodes to unvisited, coz first round
            unvisited.Add(node);

            // setting node distance to inf
            distances.Add(node, float.MaxValue);
        }

        // Set first node distance = 0
        distances[start] = 0f;

        while (unvisited.Count != 0)
        {
            // sort unvisited list by distance, ascending
            unvisited = unvisited.OrderBy(node => distances[node]).ToList();

            // get node with shortest dist
            Node current = unvisited[0];

            // remove current from unvisited
            unvisited.Remove(current);

            // when curent node == end node, break and return path
            if (current == end)
            {
                // construct shortest path
                while (previous.ContainsKey(current))
                {
                    // insert node onto final result
                    path.GetNodes.Insert(0, current);

                    // traverse from start to end
                    current = previous[current];
                }

                // insert source to final result
                path.GetNodes.Insert(0, current);
                break;

                
            }
            // looping through node connections (neighbors) and where connections are available at unvisited list
            for (int i = 0; i < current.GetConnections.Count; i++)
            {
                Node neighbor = current.GetConnections[i];

                // Getting distance between current node and connection
                float length = Vector3.Distance(current.transform.position, neighbor.transform.position);

                // The distance from start node to this connection of current node
                float alt = distances[current] + length;

                // if shorter path to connection has been found
                if (alt < distances[neighbor])
                {
                    distances[neighbor] = alt;
                    previous[neighbor] = current;
                }
            }
        }

        path.Bake();
        return path;
    }
}
