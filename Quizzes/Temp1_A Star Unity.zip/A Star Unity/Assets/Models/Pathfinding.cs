﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    public Grid grid;
    public Transform StartPos, TargetPos;
	// Use this for initialization
	void Start () {
        grid = GetComponent<Grid>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        FindPath(StartPos.position, TargetPos.position);
	}

    void FindPath(Vector3 start, Vector3 target)
    {
        Node startNode = grid.NodeFromWorldPoint(start);
        Node targetNode = grid.NodeFromWorldPoint(target);

        List<Node> openNodes = new List<Node>();
        HashSet<Node> closedNodes = new HashSet<Node>();

        // add starting node to open list, start A*
        openNodes.Add(startNode);

        // while there is something in the open list
        while (openNodes.Count > 0)
        {
            // create a node and set it to the first item in the open nodes
            Node currentNode = openNodes[0];

            // loop through open list starting from the [1]
            for (int i = 1; i < openNodes.Count; i++)
            {
                // if f cost of node is <= to the f cost of the current node
                if (openNodes[i].F_Cost < currentNode.F_Cost ||
                    openNodes[i].F_Cost == currentNode.F_Cost &&
                    openNodes[i].H_Cost < currentNode.H_Cost)
                {
                    currentNode = openNodes[i];
                }
            }

            openNodes.Remove(currentNode);
            closedNodes.Add(currentNode);

            if (currentNode == targetNode)
            {
                GetFinalPath(startNode, targetNode);
            }

            // obstacle detection, loop through each neighbor
            foreach (Node neighbor in grid.GetNeighboringNode(currentNode))
            {
                // if neighbor is not a wall and has already been checked
                if (!neighbor.isWall || closedNodes.Contains(neighbor))
                {
                    continue;
                }

                // get f cost of neighbor
                int moveCost = currentNode.G_Cost + GetManhattanDistance(currentNode, neighbor);

                // if f cost > g cost or if not in open list
                if (moveCost < neighbor.G_Cost || !openNodes.Contains(neighbor))
                {
                    neighbor.G_Cost = moveCost;
                    neighbor.H_Cost = GetManhattanDistance(neighbor, targetNode);
                    neighbor.ParentNode = currentNode;

                    if (!openNodes.Contains(neighbor))
                    {
                        openNodes.Add(neighbor);
                    }
                }
            }
            
        }
    }

    void GetFinalPath(Node start, Node end)
    {
        List<Node> finalPath = new List<Node>();
        Node currentNode = end;

        while (currentNode != start)
        {
            finalPath.Add(currentNode);
            currentNode = currentNode.ParentNode;
        }

        finalPath.Reverse();
        grid.FinalPath = finalPath;
    }

    int GetManhattanDistance(Node NodeA, Node NodeB)
    {
        int x = Mathf.Abs(NodeA.GridPosX - NodeB.GridPosX);
        int y = Mathf.Abs(NodeA.GridPosY - NodeB.GridPosY);

        return x + y; 
    }
}
