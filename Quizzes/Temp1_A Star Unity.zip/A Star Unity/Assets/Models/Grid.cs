﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {
    
    // begin pathfinding from here
    public Transform StartPosition;

    // mask that algo will look for when trying to find obstructions
    public LayerMask WallMask;

    // v2 to store dimensions of grid in world units
    public Vector2 GridWorldSize = new Vector2(30, 30);

    // determines how big each node will be
    public float NodeRadius = 0.5f;

    // distance that the nodes will spawn from each other
    public float DistanceBetweenNodes = 0;

    // Node[x,y]
    // array of nodes that the A* algo uses
    public Node[,] NodeArray;

    // completed path to traverse
    public List<Node> FinalPath;

    // 2x noderadius
    public float NodeDiameter;

    // dimensions of grid in world units
    public int GridSizeX, GridSizeY;

    private void Start()
    {
        NodeDiameter = NodeRadius * 2;
        GridSizeX = Mathf.RoundToInt(GridWorldSize.x / NodeDiameter);
        GridSizeY = Mathf.RoundToInt(GridWorldSize.y / NodeDiameter);
        NodeArray = new Node[GridSizeX, GridSizeY];
    }

    private void Update()
    {
        CreateGrid();
    }

    private void CreateGrid()
    {
        
        Vector3 bottomLeft = transform.position - Vector3.right * GridWorldSize.x / 2 - Vector3.forward * GridWorldSize.y / 2;

        // iterate through first dimension
        for (int x = 0; x < GridSizeX; x++)
        {
            // iterate through second dimension
            for (int y = 0; y < GridSizeY; y++)
            {
                Vector3 worldPoint = bottomLeft + Vector3.right * (x * NodeDiameter + NodeRadius) + Vector3.forward * (y * NodeDiameter + NodeRadius);

                bool isWall = true;

                if (Physics.CheckSphere(worldPoint, NodeRadius, WallMask))
                {
                    isWall = false;
                }

                NodeArray[x, y] = new Node(isWall, worldPoint, x, y);
            }
        }
    }

    // gt neighboring node of given node
    public List<Node> GetNeighboringNode(Node neighborNode)
    {
        List<Node> neighborList = new List<Node>();
        int checkX, checkY;

        // check right side
        checkX = neighborNode.GridPosX + 1;
        checkY = neighborNode.GridPosY;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        // check left side
        checkX = neighborNode.GridPosX - 1;
        checkY = neighborNode.GridPosY;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        // check top side
        checkX = neighborNode.GridPosX;
        checkY = neighborNode.GridPosY + 1;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        // check bottom side
        checkX = neighborNode.GridPosX;
        checkY = neighborNode.GridPosY - 1;

        if (checkX >= 0 && checkX < GridSizeX)
        {
            if (checkY >= 0 && checkY < GridSizeY)
            {
                neighborList.Add(NodeArray[checkX, checkY]);
            }
        }

        return neighborList;
    }

    // gets closest node to world pos
    public Node NodeFromWorldPoint(Vector3 worldPos)
    {
        float xpos = ((worldPos.x + GridWorldSize.x / 2) / GridWorldSize.x);
        float ypos = ((worldPos.z + GridWorldSize.y / 2) / GridWorldSize.y);

        xpos = Mathf.Clamp01(xpos);
        ypos = Mathf.Clamp01(ypos);

        int x = Mathf.RoundToInt((GridSizeX - 1) * xpos);
        int y = Mathf.RoundToInt((GridSizeY - 1) * ypos);

        return NodeArray[x, y];
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(GridWorldSize.x, 1, GridWorldSize.y));
        if (NodeArray != null)
        {
            foreach (Node n in NodeArray)
            {
                if (n.isWall)
                {
                    Gizmos.color = Color.white;
                }
                else
                {
                    Gizmos.color = Color.yellow;
                }

                if (FinalPath != null)
                {
                    if (FinalPath.Contains(n))
                    {
                        Gizmos.color = Color.red;
                    }
                }

                Gizmos.DrawCube(n.position, Vector3.one * (NodeDiameter - DistanceBetweenNodes));
            }
        }
    }
}
