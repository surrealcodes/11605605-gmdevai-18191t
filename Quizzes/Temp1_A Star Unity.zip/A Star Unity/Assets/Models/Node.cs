﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    // pos in node array
    public int GridPosX;
    public int GridPosY;

    // if object is wall
    public bool isWall;

    // world pos of nodee
    public Vector3 position;
    // Use this for initialization

    // stores previous node that it came from
    // calculate shortest path
    public Node ParentNode;

    // cost of moving to next node
    public int G_Cost;

    // heuristic (distance to the goal from this node)
    public int H_Cost;

    // quick getter function to add g and h
    public int F_Cost
    {
        get
        {
            return G_Cost + H_Cost;
        }
    }

    public Node(bool isWall, Vector3 worldPos, int gridX, int gridY)
    {
        this.isWall = isWall;
        this.position = worldPos;
        this.GridPosX = gridX;
        this.GridPosY = gridY;
    }
}
