﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMove : MonoBehaviour {

    public Transform TargetPos;
    public float Speed;

    private Vector3 origPos;
    private Vector3 tempPos;
    private float direction;
	// Use this for initialization
	void Start () {
        origPos = transform.position;
        direction = 1.0f;
    }
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Distance(transform.position, TargetPos.position) == 0)
        {
            tempPos = origPos;
        }

        if (Vector3.Distance(transform.position, origPos) == 0)
        {
            tempPos = TargetPos.position;
        }

        transform.position = Vector3.MoveTowards(transform.position, tempPos, Time.deltaTime * Speed);

    }
}
