﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    public Grid grid;
    public Transform StartPos;
    public Transform TargetPos;

    private List<Node> finalPath;

	// Use this for initialization
	void Start () {
        finalPath = new List<Node>();
        finalPath = grid.FinalPath;
        transform.position = StartPos.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
