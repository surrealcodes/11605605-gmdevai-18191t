﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The Follower.
/// </summary>
[ExecuteInEditMode]
public class Follower : MonoBehaviour
{
    public Text HPText;
    public Text SpdText;
	[SerializeField]
	protected Graph m_Graph;
	[SerializeField]
	protected Node m_Start;
	[SerializeField]
	protected Node m_End;
	[SerializeField]
	protected float m_Speed = 0.01f;
    protected float og_Speed;
	protected Path m_Path = new Path ();
	protected Node m_Current;

    [SerializeField]
    protected int hp;
    public int maxhp;

    private bool inT2;
    private bool inT1;

	void Start ()
	{
		m_Path = m_Graph.GetShortestPath ( m_Start, m_End );
        hp = maxhp/2;
        og_Speed = m_Speed;
        inT2 = false;
        inT1 = false;

        Follow(m_Path);
        
	}

	/// <summary>
	/// Follow the specified path.
	/// </summary>
	/// <param name="path">Path.</param>
	public void Follow ( Path path )
	{
		StopCoroutine ( "FollowPath" );
        hp = maxhp/2;
        inT2 = false;
        inT1 = false;
        m_Path = path;
		transform.position = m_Path.nodes [ 0 ].transform.position;
		StartCoroutine ( "FollowPath" );
	}

	/// <summary>
	/// Following the path.
	/// </summary>
	/// <returns>The path.</returns>
	IEnumerator FollowPath ()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.update += Update;
		#endif
		var e = m_Path.nodes.GetEnumerator ();
		while ( e.MoveNext () )
		{
			m_Current = e.Current;
			
			// Wait until we reach the current target node and then go to next node
			yield return new WaitUntil ( () =>
			{
				return transform.position == m_Current.transform.position;
			} );
		}
		m_Current = null;
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.update -= Update;
		#endif
	}

	void Update ()
	{
        if (m_Current != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_Current.transform.position, m_Speed);
        }

        if (hp <= 0) gameObject.SetActive(false);

        HPText.text = "HP: " + hp.ToString() + "/" + maxhp.ToString();
        SpdText.text = "SPD: " + m_Speed.ToString();
	}

    public void TakeDmg (int dmg)
    {
        hp -= dmg;
        if (hp - dmg <= 0)
        {
            hp = 0;
        }
    }	

    public void Heal (int heal)
    {
        hp += heal;
        if (hp > maxhp)
        {
            hp = maxhp;
        }
    }

    public void ChangeSpeed (float change)
    {
        m_Speed = change;
    }

    public void RevertSpeed ()
    {
        m_Speed = og_Speed;
    }

    public bool IsHealthFull()
    {
        return hp >= maxhp;
    }

    public bool IsDead()
    {
        return hp <= 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Entered");
        if (collision.tag == "T3")
        {
            GetComponent<SpriteRenderer>().color = new Color (0.5f, 0.5f, 1);
            ChangeSpeed(m_Speed + 0.02f);
        }
        else if (collision.tag == "T2")
        {
            inT2 = true;
            GetComponent<SpriteRenderer>().color = new Color (0.5f, 1 , 0.5f);
            StartCoroutine(ContinuousHeal(this));
        }
        else if (collision.tag == "T1")
        {
            inT1 = true;
            GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0.5f);
            ChangeSpeed(m_Speed - 0.01f);
            StartCoroutine(ContinuousDmg(this));
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("Exited");
        if (collision.tag == "T2")
        {
            inT2 = false;            
            StopCoroutine(ContinuousHeal(this));
        }
        else if (collision.tag == "T1")
        {
            inT1 = false;
            StopCoroutine(ContinuousDmg(this));
        }

        RevertSpeed();
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    public IEnumerator ContinuousHeal(Follower player)
    {
        while (inT2 && !player.IsHealthFull())
        {
            player.Heal(1);
            yield return new WaitForSeconds(1);
        }
    }

    public IEnumerator ContinuousDmg(Follower player)
    {
        while (inT1 && !player.IsDead())
        {
            player.TakeDmg(1);
            yield return new WaitForSeconds(1);
        }
    }
}
