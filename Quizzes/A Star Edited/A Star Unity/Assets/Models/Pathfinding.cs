﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    public Grid grid;
    public Transform startPos, targetPos;
    public float Speed;
    private int count;
    private bool completedTraversal;
    private void Awake()
    {
        grid = GetComponent<Grid>();
        completedTraversal = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        FindPath(startPos.position, targetPos.position);
	}

    void FindPath(Vector3 start, Vector3 target)
    {
        Node startNode = grid.NodeFromWorldPoint(start);
        Node targetNode = grid.NodeFromWorldPoint(target);

        List<Node> openNodes = new List<Node>();
        HashSet<Node> closedNodes = new HashSet<Node>();

        openNodes.Add(startNode); //add the start node to the open list to begin the algorithm

        while (openNodes.Count > 0) // while there is something in the open list
        {
            Node currentNode = openNodes[0]; //create a node and set it to the first item in the open nodes

            //loop through the open list starting from the second object
            for (int i = 1; i < openNodes.Count; i++)
            {
                // if the F Cost of that object is less than or equal to the F cost of the current node
                if (openNodes[i].F_Cost < currentNode.F_Cost ||
                    openNodes[i].F_Cost == currentNode.F_Cost &&
                    openNodes[i].H_Cost < currentNode.H_Cost)
                {
                    currentNode = openNodes[i]; //set the current node to that object
                }
            }

                openNodes.Remove(currentNode);
                closedNodes.Add(currentNode);

                if (currentNode == targetNode)
                {
                    GetFinalPath(startNode, targetNode);
                }

                // obstacle detection
                foreach (Node neighbor in grid.GetNeighboringNodes(currentNode)) // loop through each neighbor of the current node
                {
                    // if the neighbor is not a wall or has already been checked
                    if (!neighbor.IsWall || closedNodes.Contains(neighbor))
                    {
                        continue; //skip node
                    }

                    int moveCost = currentNode.G_Cost + GetManhattanDistance(currentNode, neighbor); // Get the F Cost of the neighbor

                    if (moveCost < neighbor.G_Cost || !openNodes.Contains(neighbor)) // if the F Cost is greater than the G Cost or it is not in the open list
                    {
                        neighbor.G_Cost = moveCost;
                        neighbor.H_Cost = GetManhattanDistance(neighbor, targetNode);
                        neighbor.ParentNode = currentNode;

                        if (!openNodes.Contains(neighbor))
                        {
                            openNodes.Add(neighbor);
                        }
                    }
                }
            
        }
    }

    void GetFinalPath(Node start, Node end)
    {
        List<Node> finalPath = new List<Node>();
        Node currentNode = end;

        while (currentNode != start)
        {
            finalPath.Add(currentNode);
            currentNode = currentNode.ParentNode;  
        }

        finalPath.Reverse();

        if (count < finalPath.Count)
        {
            Node targetNode = finalPath[count];
            startPos.transform.position = Vector3.MoveTowards(startPos.position, targetNode.Position, Time.deltaTime * Speed);

            if (Vector3.Distance(startPos.transform.position, targetNode.Position) == 0)
            {
                if (targetNode != end)
                {
                    count++;
                }
            }
        }

        grid.FinalPath = finalPath;
    }

    int GetManhattanDistance(Node nodeA, Node nodeB)
    {
        int x = Mathf.Abs(nodeA.GridPosX - nodeB.GridPosX); //x1 - x2
        int y = Mathf.Abs(nodeA.GridPosY - nodeB.GridPosY); //y1 - y2

        return x + y;
    }
}
