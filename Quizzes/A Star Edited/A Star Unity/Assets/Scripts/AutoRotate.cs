﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour {

    public bool LeftSpin;
    public bool RightSpin;
    public float RotateVal;

    private bool isSpinning;
    private Vector3 direction;
	// Use this for initialization
	void Start () {

        isSpinning = true;

        if (LeftSpin)
        {
            direction = Vector3.up;
        }  
        else if (RightSpin)
        {
            direction = Vector3.down;
        }
        else if (LeftSpin && RightSpin)
        {
            isSpinning = false;
            throw new System.Exception("Cannot have both LeftSpin and RightSpin as true");            
        }
        else if (!(LeftSpin && RightSpin))
        {
            isSpinning = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (isSpinning)
        {
            transform.Rotate(direction, RotateVal * Mathf.Rad2Deg * Time.deltaTime);
        }
	}
}
