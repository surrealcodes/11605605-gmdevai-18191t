﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/Decisions/HealthDecision")]
public class HealthDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool isHealthy = CheckHealth(controller);
        return isHealthy;
    }

    public bool CheckHealth(StateController controller)
    {
        return (controller.tankHealth.GetCurrentHealth() > 50);
    }
}
