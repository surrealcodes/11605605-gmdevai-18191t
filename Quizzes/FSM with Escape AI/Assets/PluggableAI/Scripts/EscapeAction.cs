﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/Actions/Escape")]
public class EscapeAction : Action {

    public override void Act(StateController controller)
    {
        Escape(controller);
    }

    public void Escape(StateController controller)
    { 
        controller.navMeshAgent.destination = (controller.currentPosition.position-controller.chaseTarget.position)*3;
        controller.navMeshAgent.Resume();

        //if (Vector3.Distance(controller.currentPosition.position, controller.chaseTarget.position) < 10.0f)
        //{
        //    controller.navMeshAgent.velocity *= 1.0f;
        //}
    }
}
